FROM python:3.6
ENV PYTHONUNBUFFERED 1

WORKDIR /code
COPY . /code/
RUN pip install pipenv
RUN pipenv install --system

CMD python cli.py -i 0.0.0.0 -p 5000
