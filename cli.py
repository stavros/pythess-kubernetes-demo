#!/usr/bin/env python

from flask import Flask

app = Flask(__name__)


@app.route("/")
def home():
    return "Welcome."


def main():
    app.run(host="0.0.0.0", port=5000, debug=True)


if __name__ == "__main__":
    main()
